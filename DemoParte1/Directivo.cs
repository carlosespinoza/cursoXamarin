﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoParte1
{
    public class Directivo:Persona
    {
        public Directivo(string nombre, string apellidos) : base(nombre, apellidos)
        {
        }

        public string TipoContrato { get; set; }
    }
}
