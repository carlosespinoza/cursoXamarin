﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoParte1
{
    public class Alumno:Persona
    {
        public Alumno(string nombre, string apellidos) : base(nombre, apellidos)
        {
        }

        public int Matricula { get; set; }
        public string Carrera { get; set; }

        public bool HayReprobadas(out int cantidad )
        {
            cantidad = 5;
            return true;
        }

        public int CantidadDeReprobradas()
        {
            return 5;
        }

        public List<string> ListaDeMateriasReprobadas()
        {
            return null;
        }
        /// <summary>
        /// Obtiene la cantidad de materias reprobadas siendo la calificación minima de 70
        /// </summary>
        /// <returns>Cantidad de materias reprobadas</returns>
        public int Reprobadas()
        {
        //Aridad=0 -> número de parametros de un metodo
            return 5;
        }
        /// <summary>
        /// Indica si hay materias reprobadas indicandole la calificación minima
        /// </summary>
        /// <param name="calMinima">Calificación minima para aprobar una materia</param>
        /// <returns>Indica si hay o no materias reprobadas</returns>
        public bool Reprobadas(int calMinima)
        {
            return true;
        }
    }
}
