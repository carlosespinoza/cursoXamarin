﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoParte1
{
    /// <summary>
    /// Clase para un Docente
    /// </summary>
    public interface IDocente
    {
        /// <summary>
        /// Metodo que ejecuta el procedimiento de DarClases
        /// </summary>
        /// <param name="materia">Id de la materia</param>
        void DarClases(string Materia);
        void EntregarDocumentacion(string materia);
        int PorcentaDeAprobacion();
        
    }
}
