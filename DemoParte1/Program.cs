﻿using System;

namespace DemoParte1
{
    class Program
    {
        static void Main(string[] args)
        {
            Alumno juanito = new Alumno("Juanito","Perez");
            Console.WriteLine(juanito);
            int cantidad = 0;
            if(juanito.HayReprobadas(out cantidad))
            {
                Console.WriteLine("Hay un total de " + cantidad + " reprobadas");
            }
            int n=juanito.Reprobadas();
            Docente mario = new Docente("Mario", "Escalante");
            Contratar<Docente> contrato = new Contratar<Docente>(mario);
            contrato.FirmarContrato();
            contrato.Despedir();
            Contratar<Directivo> contrato2 = new Contratar<Directivo>(new Directivo("Juan", "Licona"));
            contrato2.FirmarContrato();
            contrato2.Despedir();


            
            

        }
    }
}
