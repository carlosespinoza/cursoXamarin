﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace DemoParte1
{
    /// <summary>
    /// Clase abstracta para definir una persona
    /// </summary>
    public abstract class Persona
    {
        /*
         *  Variables-> camelCase
         *  Metodos, propiedades ->PascalCase
         *  variable privada => _
         *  LblFecha
         *  PkrSexo
         *  EntCorreo
         * 
         * */

        private int _departamento;

        public DateTime FechaNacimiento { get; set; }
        /// <summary>
        /// Indica si la persona esta activa
        /// </summary>
        public bool EstaActivo { get; set; }
        public string Nombre { get; set; }
        /// <summary>
        /// Apellidos de la persona
        /// </summary>
        public string Apellidos { get; set; }
        /// <summary>
        /// Contrustor de la clase
        /// </summary>
        /// <param name="nombre">Nombre de la persona</param>
        /// <param name="apellidos">Apellidos de la persona</param>
        public Persona(string nombre, string apellidos)
        {
            //Este es el contrustor que solicita el nombre y el apellido de la persona
            Nombre = nombre;
            Apellidos = apellidos;
            Debug.Print("Estoy en el constructor");
        }

        public override string ToString()
        {
            return $"{Nombre} {Apellidos}";
        }
        ~Persona()
        {
            Console.WriteLine($"Se murio: {Nombre}");
        }
    }
}
