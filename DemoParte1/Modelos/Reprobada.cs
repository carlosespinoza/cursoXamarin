﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoParte1.Modelos
{
    public class Reprobada
    {
        public bool HayReprobadas { get; set; }
        public int Cantidad { get; set; }
    }
}
