﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoParte1
{
    public class Contratar<T> where T:Persona
    {
        public T Persona { get; set; }
        public Contratar(T persona)
        {
            Persona = persona;
        }
        public void FirmarContrato()
        {
            Console.WriteLine($"{Persona.ToString()} que es un {typeof(T).Name} firmo contrato el {DateTime.Now.ToShortDateString()}");
        }
        public void Despedir()
        {
            Console.WriteLine("Hoy se despidio a " + Persona.ToString());
        }
    }
}
