﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoParte1
{
    public class Docente:Persona, IDocente
    {
        public Docente(string nombre, string apellidos) : base(nombre, apellidos)
        {
        }

        public int Contrato { get; set; }

        public void DarClases(string materia)
        {
            throw new NotImplementedException();
        }

        public void EntregarDocumentacion(string materia)
        {
            throw new NotImplementedException();
        }

        public int PorcentaDeAprobacion()
        {
            throw new NotImplementedException();
        }
    }
}
